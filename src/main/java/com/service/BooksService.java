package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bean.Books;
import com.dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksDao;
	
	//Store The Book Details
	public String storeBookDetails(Books book) {
		if(!booksDao.existsById(book.getId())) {
			booksDao.save(book);
			return "book Details stored sucessfully";
		}else {
			return "book Details already present";
		}
	}
	
	//Update Book Price By ID
	public String updateBook(Books book) {
		if(!booksDao.existsById(book.getId())) 
		{
		    return "Book is not present with the given id";
		}
		else 
		{
			Books b=booksDao.getById(book.getId());
  		  b.setAuthor(book.getAuthor());
  		  
  		  b.setPrice(book.getPrice());
  		  b.setTitle(book.getTitle());
			booksDao.saveAndFlush(b);				
			return "Book details updated successfully";
		}	
	}
	
	public List<Books> getAllBooksAvaliable(){
		return booksDao.findAll();
	}
	
	//Get Book Details By Giving Id
	public Books findBooksById(int id) {
		if(booksDao.existsById(id)) {
			return booksDao.findById(id).get();
		}else {
			return null;
		}
		
	}
	
	//Deleted Book By ID
	public String deleteBookById(int id) {
		if(!booksDao.existsById(id)) {
			return "Book Details Not Present";
		}else {
			booksDao.deleteById(id);
			return "Book Deleted Sucessfully";
		}
	}
	
	
}
